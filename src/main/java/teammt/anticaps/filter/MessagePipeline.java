package teammt.anticaps.filter;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

public class MessagePipeline extends Registerable {

    public MessagePipeline(MLib lib) {
        super(lib);
    }

    /**
     * This method will return what percentage (0.0-100.0) of the
     * message is in caps.
     * 
     * @param message - The message to check
     * @return - The percentage of the message that is in caps
     */
    public double countPercentage(String message) {
        int total = message.length();
        int caps = 0;
        for (char c : message.toCharArray()) {
            if (Character.isUpperCase(c))
                caps++;
        }
        return ((double) caps / (double) total) * 100.0;
    }

    private String decapitalize(String word) {
        boolean firstCapital = Character.isUpperCase(word.charAt(0));

        if (firstCapital)
            return word.charAt(0) + word.substring(1).toLowerCase();
        else
            return word.toLowerCase();
    }

    public String filter(String message) {
        double perc = countPercentage(message);
        if (perc < getRatio())
            return message;
        if (message.length() < getLowerLimit())
            return message;

        // Go backwards, decapitalizing words as we go
        String[] words = message.split(" ");
        int index = words.length - 1;
        while (perc > getRatio() && index >= 0) {
            words[index] = decapitalize(words[index]);
            perc = countPercentage(String.join(" ", words));
            index--;
        }

        return String.join(" ", words);
    }

    public double getRatio() {
        return lib.getConfigurationAPI().getConfig().getDouble("ratio");
    }

    public int getLowerLimit() {
        return lib.getConfigurationAPI().getConfig().getInt("allow-messages-under");
    }
}
