package teammt.anticaps.filter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

public class PipelineApplier extends Registerable {
    private MessagePipeline pipeline;

    public PipelineApplier(MLib lib, MessagePipeline pipeline) {
        super(lib);
        this.pipeline = pipeline;
    }

    @EventHandler
    public void onAsyncChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        if (bypasses(event.getPlayer()))
            return;

        String filtered = pipeline.filter(message);
        if (!filtered.equals(message)) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(lib.getPlugin(), () -> {
                lib.getMessagesAPI().sendMessage("filter-applied", event.getPlayer());
            }, 1);
        }

        event.setMessage(filtered);

    }

    private boolean bypasses(Player player) {
        String permission = lib.getConfigurationAPI().getConfig().getString("bypass-permission");
        return player.hasPermission(permission);
    }

}
