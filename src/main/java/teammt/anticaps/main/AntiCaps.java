package teammt.anticaps.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.anticaps.filter.MessagePipeline;
import teammt.anticaps.filter.PipelineApplier;

public class AntiCaps extends JavaPlugin {
    private MLib lib;

    private MessagePipeline pipeline;

    @Override
    public void onEnable() {
        this.lib = new MLib(this);
        this.lib.getConfigurationAPI().requireAll();

        this.pipeline = new MessagePipeline(lib);
        this.pipeline.register();

        new PipelineApplier(lib, pipeline).register();
    }
}
